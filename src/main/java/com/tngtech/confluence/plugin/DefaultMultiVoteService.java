package com.tngtech.confluence.plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.Map;
import java.util.HashMap;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheFactory;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.renderer.PageContext;

import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.velocity.htmlsafe.HtmlSafe;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.v2.macro.MacroException;
import org.apache.commons.lang.StringUtils;

import com.atlassian.confluence.cluster.ClusterManager;
import com.atlassian.confluence.cluster.ClusteredLock;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.user.User;
import com.tngtech.confluence.plugin.data.ItemKey;
import com.tngtech.confluence.plugin.data.VoteItem;

import javax.xml.stream.XMLStreamException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class DefaultMultiVoteService implements MultiVoteService {
	
    public VoteItem recordInterest(String remoteUser, boolean requestUse, ItemKey key) {
        ClusteredLock lock = getLock(key);
        Set<String> users;
        try {
            lock.lock();
            users = doRecordInterest(remoteUser, requestUse, key);
        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
        return new VoteItem(key.getItemId(), users);
    }

    @Override
    public void reset(ContentEntityObject page, String tableId, List<String> itemIds) {
        for (String itemId: itemIds) {
            ItemKey key = new ItemKey(page, tableId, itemId);

            ClusteredLock lock = getLock(key);

            try {
                lock.lock();
                String property = buildPropertyString(key);
                contentPropertyManager.removeProperty(key.getPage(), property);
            } finally {
                if (lock != null) {
                    lock.unlock();
                }
            }
        }
    }

    private ClusteredLock getLock(ItemKey key) {
        return clusterManager.getClusteredLock("multivote.lock." + key.getTableId() + "." + key.getItemId());
    }

    private Set<String> doRecordInterest(String user, Boolean requestUse, ItemKey key) {
        boolean changed;
        Set<String> users = retrieveAudience(key);
        if (requestUse && canStillVote(user, key)) {
            changed = users.add(user);
        } else {
            changed = users.remove(user);
        }
        if (changed) {
            persistAudience(key, users);
        }
        return users;
    }
    
    private boolean canStillVote(String user, ItemKey key) {
    	int voteCount = getUserVoteCount(user, key);
    	int maxVotes = getMaxVotes(key);
    	return maxVotes <= 0|| voteCount < maxVotes;
    }
    
    private int getUserVoteCount(String user, ItemKey key) {
    	int voteCount = 0;
    	ContentEntityObject page = key.getPage();
    	String tableId = key.getTableId();
    	Set<String> itemIds = parseItemIdsFromContentBody(page.getBodyAsString());
    	for (String itemId : itemIds) {
    		ItemKey choice = new ItemKey(page, tableId, itemId);
    		Set<String> audience = retrieveAudience(choice);
    		if (audience.contains(user)) {
    			voteCount++;
    		}
    	}
    	return voteCount;
    }
    
    private Set<String> parseItemIdsFromContentBody(String contentBody) {
    	Set<String> itemIds = new TreeSet<String>();
    	try {
	    	Document doc = Jsoup.parse(contentBody);
	    	Element table = doc.select("table").get(0);
	    	Elements rows = table.select("tr");
	    	for (int i = 1; i < rows.size(); i++) { //skip header row
				Element firstCol = rows.get(i).select("td").get(0);
				itemIds.add(firstCol.text());
			}
	    	return itemIds;
    	} catch (IllegalArgumentException e) {
    		return itemIds;
    	}
    }
    
    private int getMaxVotes(ItemKey key) {
    	ContentEntityObject page = key.getPage();
    	int maxVotes = 0;
    	try {
    		Document doc = Jsoup.parse(page.getBodyAsString());
    		Element maxVotesElement = doc.select("ac|parameter[ac:name=\"maxVotes\"]").get(0);
    		maxVotes = new Integer(maxVotesElement.text()).intValue();
    	} catch (Exception e) {
    		
    	}
    	
    	return maxVotes;
    }

    public Set<String> retrieveAudience(ItemKey key) {
        String usersAsString = contentPropertyManager.getTextProperty(key.getPage(), buildPropertyString(key));
        if (usersAsString == null) {
            usersAsString = "";
        }
        Set<String> users = new TreeSet<String>();
        StringTokenizer userTokenizer = new StringTokenizer(usersAsString, ",");
        while (userTokenizer.hasMoreTokens()) {
            users.add(userTokenizer.nextToken().trim());
        }
        return users;
    }

    private void persistAudience(ItemKey key, Set<String> users) {
        String property = buildPropertyString(key);
        contentPropertyManager.setTextProperty(key.getPage(), property, StringUtils.join(users, ", "));
    }

    private String buildPropertyString(ItemKey key) {
        return "multivote." + key.getTableId() + "." + key.getItemId();
    }

    public String getUserFullNamesAsString(Set<String> audience) {
        List<String> fullNames = new ArrayList<String>();

        for (String userName: audience) {
            fullNames.add(getFullName(userName));
        }
        return StringUtils.join(fullNames, ", ");
    }

    @HtmlSafe
    public String getUserFullNamesAsHtml(Set<String> audience, PageContext context) throws MacroException {
        List<String> userLinks = new ArrayList<String>(audience.size());

        for (String userName: audience) {
            userLinks.add(getUserLink(userName, context));
        }

        return StringUtils.join(userLinks, ", "); // TODO configurable
    }

    private String getUserLink(String userName, PageContext context) throws MacroException {
        final Cache cache = cacheFactory.getCache("com.tngtech.confluence.plugin.multivote.userLink");
        String result = (String)cache.get(userName);
        if (result == null) {
            Map<String, Object> contextMap = new HashMap<String, Object>();
            contextMap.put("userName", userName);

            try {
                result = xmlXhtmlContent.convertStorageToView(
                        VelocityUtils.getRenderedTemplate("templates/extra/userlink.vm", contextMap),
                        new DefaultConversionContext(context)
                );
            } catch (Exception e) {
                throw new MacroException(e);
            }

            cache.put(userName, result);
        }

        return result;
    }

    private String getFullName(String userName) {
        String fullName = userName;
        User user = userAccessor.getUser(userName);
        if (user != null) {
            fullName = user.getFullName();
        }
        if (fullName == null) {
            fullName = userName;
        }
        return fullName;
    }

    /*
     * injected Services
     */
    private ContentPropertyManager contentPropertyManager;
    private UserAccessor userAccessor;
    private ClusterManager clusterManager;
    private XhtmlContent xmlXhtmlContent;
    private CacheFactory cacheFactory;

    public void setContentPropertyManager(ContentPropertyManager contentPropertyManager) {
        this.contentPropertyManager = contentPropertyManager;
    }

    public void setUserAccessor(UserAccessor userAccessor) {
        this.userAccessor = userAccessor;
    }

    public void setClusterManager(ClusterManager clusterManager) {
        this.clusterManager = clusterManager;
    }

    public void setXmlXhtmlContent(XhtmlContent xmlXhtmlContent) {
        this.xmlXhtmlContent = xmlXhtmlContent;
    }

    public void setCacheFactory(CacheFactory cacheFactory) {
        this.cacheFactory = cacheFactory;
    }
}
