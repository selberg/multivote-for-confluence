package com.tngtech.confluence.plugin.data;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "interested")
@XmlAccessorType(XmlAccessType.FIELD)
public class VoteResponse {
    @XmlElement(name = "id")
    private String id;
    @XmlAttribute
    private String users;
    @XmlAttribute
    private String htmlUsers;
    @XmlAttribute
    private int userNo;
    @XmlAttribute
    private boolean interested;

    public VoteResponse() {

    }
   
    public VoteResponse(String id, String users, String htmlUsers, int userNo, boolean interested) {
        this.id = id;
        this.users = users;
        this.userNo = userNo;
        this.htmlUsers = htmlUsers;
        this.interested = interested;
    }

    public String getId() {
        return id;
    }

    public String getUsers() {
        return users;
    }

    public String htmlUsers() {
        return htmlUsers;
    }

    public int getUserNo() {
        return userNo;
    }
    
    public boolean getInterested() {
    	return interested;
    }
}
